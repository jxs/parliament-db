from django.contrib import admin

# Register your models here.
from .models import Member, Party, Legislature, Connection, Role, Company

admin.site.register(Legislature)
admin.site.register(Party)
admin.site.register(Member)
admin.site.register(Role)
admin.site.register(Connection)
admin.site.register(Company)
