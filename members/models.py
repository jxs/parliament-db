from django.db import models


class Legislature(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField(null=True)

    def __str__(self): return self.name


class Party(models.Model):
    name = models.CharField(max_length=200)
    logo = models.ImageField(null=True, blank=True)

    def __str__(self): return self.name


class Member(models.Model):
    name = models.CharField(max_length=200)
    party = models.ForeignKey(Party)
    picture = models.ImageField()

    def __str__(self): return self.name


class Role(models.Model):
    name = models.CharField(max_length=200)
    government = models.BooleanField()
    legislature = models.ForeignKey(Legislature)
    member = models.ForeignKey(Member)

    def __str__(self): return self.name


class Company(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    logo = models.ImageField(null=True, blank=True)

    def __str__(self): return self.name


class Connection(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    member = models.ForeignKey(Member)
    reference = models.URLField(blank=True, null=True)
    company = models.ForeignKey(Company, null=True, blank=True)

    def __str__(self): return self.name
