# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='connection',
            name='member',
            field=models.ForeignKey(default=1, to='members.Member'),
            preserve_default=False,
        ),
    ]
