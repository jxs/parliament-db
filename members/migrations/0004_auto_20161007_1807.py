# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20161007_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='connection',
            name='reference',
            field=models.URLField(null=True, blank=True),
        ),
    ]
