# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_connection_member'),
    ]

    operations = [
        migrations.AddField(
            model_name='connection',
            name='reference',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='legislature',
            name='end_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='party',
            name='logo',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
