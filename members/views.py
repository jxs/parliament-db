from django.shortcuts import render
from django.http import HttpResponse
from .models import Member, Connection

# def index(request):
#     hostname = os.getenv('HOSTNAME', 'unknown')
#     PageView.objects.create(hostname=hostname)

#     return render(request, 'welcome/index.html', {
#         'hostname': hostname,
#         'database': database.info(),
#         'count': PageView.objects.count()
#     })


def health(request):
    return HttpResponse("200")


def indexr(request):
    return render(request, 'index.html')


def search(request):
    term = request.GET.get('term', '')
    connections = Connection.objects.filter(description__search=term)
    return render(request, 'index.html', {'connections': connections, 'term': term})
